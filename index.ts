import * as Discord from 'discord.js';
import * as CONFIG from "./json/bot_config.json";
import { ServerConfigHandler } from "./modules/server_config";
import { CommandParser, Tokenized } from "./modules/command_parser";

const Client = new Discord.Client();
const ServerConfig = new ServerConfigHandler();
const Parser = new CommandParser();

Client.login(CONFIG.TOKEN);

Client.on('ready', () => {
    console.log('Logged in as ' + Client.user?.tag + '.');
});

Client.on('message', msg => Parser.ParseMessage(msg, () => {

}));

Client.on('channelDelete', ServerConfig.checkChannelDelete);