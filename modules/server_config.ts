import * as fs from 'fs';
import { Guild, Channel, GuildChannel, DMChannel } from 'discord.js';

export class ServerConfigHandler {
    public servers: { [key: string]: ServerConfig } = {};
    constructor() {    }

    getConfig(guild: Guild) {
        if (this.servers[guild?.id] == undefined) {
            this.servers[guild?.id] = new ServerConfig(guild);
        }
        return this.servers[guild?.id];
    }

    setPrefixMode() {

    }

    setCommandChannel() {

    }

    checkChannelDelete(channel: Channel) {
        if (channel instanceof GuildChannel && channel.type === "text") {
            if (!this.servers[channel.guild.id].usePrefixMode && this.servers[channel.guild.id].readFromChannelID === channel.id && channel.deleted) {
                this.servers[channel.guild.id].usePrefixMode == true;
                this.servers[channel.guild.id].readFromChannelID == "";
            }
        }
    }
}

export class ServerConfig {
    public prefix: string = "goose";
    public usePrefixMode: boolean = true;
    public readFromChannelID: string = "";
    constructor(public guild: Guild) {
        
    }

}