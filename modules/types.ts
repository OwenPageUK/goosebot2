export interface commandConfig {
    alias: string
}

export class Command {
    public alias: string;
    constructor(config: commandConfig) {
        this.alias = config.alias.toUpperCase();
    }

    RUN(): number { return 0; }
}